<?php

/**
 * Class BlockProfilerPanelsRendererIpe
 */
class BlockProfilerPanelsRendererEditor extends panels_renderer_editor {

  /**
   * Override parent::render_panes() to add timers to individual pane rendering.
   */
  function render_panes() {
    ctools_include('content');
    $this->rendered['panes'] = array();
    foreach ($this->prepared['panes'] as $pid => $pane) {
      $pane_label = 'pane: ' . $pane->type . ':' . $pane->subtype;
      timer_start($pane_label);
      $content = $this->render_pane($pane);
      if ($content) {
        $this->rendered['panes'][$pid] = $content;
      }
      timer_stop($pane_label);
    }
    return $this->rendered['panes'];
  }

}
